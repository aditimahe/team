# Final Reports

1. [Example Project Reports](#example-project-reports)
2. [Instructions](#instructions)

## 1. Example Project Reports

### 2021 Summer

- [Life Coach Skill for `qary`](./reports/) by [Kalika Curry](https://www.linkedin.com/in/kalikakay/)
- [Robust NLP](./reports) by [Kazuma Endo](https://www.linkedin.com/in/kazuma-endo/)
- [Book Summary NLP for Genre Prediction](./reports/) by [Martha Gavidia](https://www.linkedin.com/in/martha-gavidia-a427281a4/)
- [Twitter Hate Speech Detection](./reports/) by [Makeda Gebremedhin](https://www.linkedin.com/in/makedagebremedhin/)
- [Agile Project Planning and ALT Text for Accessibility](./reports/) by [Constance Mlagami](https://constancea51.substack.com/p/coming-soon)
- [Pulse of the World for `qary`](./reports/) by [Copeland Royall](https://www.linkedin.com/in/copeland-r-335b4b19b/)
- [Chatbot development experiments](./reports/Greg_Chatbot_Development_Internship_Report.md) by [Greg Thompson](https://canvas.instructure.com/eportfolios/29499/Portfolio_Home)

### 2021 Winter

- [Quote Recommender](./reports/billy-quote-recommender.md) by Billy Horn (Winter 2021)
- [Word Map](./reports/jon-wordmap.ipynb) by Jon Sundin (Winter 2021)
- [Vaccination Uptake](./reports/winston-vaccination-report.docx) by Winston Oakley (Winter 2021)
- [Rap Recommender notebook](./reports/Rap_recommender.ipynb) and [report](Uzi_final_report) by Uzi Ikwakor (Winter 2021)
- [Neural Machine Translation](./reports/hanna-project-report-neural-machine-translation.ipynb) by Hanna Seyoum (Winter 2021)
- [History Tutor skill for qary](https://gitlab.com/tangibleai/qary/-/merge_requests/73/diffs) by Vishvesh Bhat

### 2021 Expert Volunteers

- [Deterministic Dialog Engine (FSM)](https://gitlab.com/tangibleai/qary/-/blob/master/src/qary/skills/quiz.py) by Jose Robins
- [NMT hyperparameter tuning and PyPi package](https://gitlab.com/tangibleai/machine-translation/-/blob/master/README.md)
by Winnie Yeung

### 2020 Fall

- _Your First Rasa Chatbot_ tutorial by Eda Firat
- [Spelling corrector for Question Answering](https://gitlab.com/tangibleai/qary/-/blob/master/src/qary/skills/corrected_qa.py) by Travis Harper

## 2. Instructions

Early on in your project you can draft your final report in Jupyter Notebook (`.ipynb`), markdown (`.md`), `.docx` and/or `.pptx`. Read-only or proprietary formats like `.pdf` are not ideal, because others can't build on your work. You can flesh out your report over the course of the project according to the instructions belos. And be sure to share it with the team by pushing it up to gitlab each time you update it: https://gitlab.com/tangibleai/team/-/blob/master/project-reports/reports

There are typically 3 kinds of project reports that work well for Tangible AI interns:

- 2.1 Data science or analytics
- 2.2 Software or web app development
- 2.3. Conversation design or chatbot development

### 2.1. Data projects

A typical data project report should include 6 sections. These could each be sections in a markdown document or jupyter notebook. If you're doing a lightning talk at the SD Python User Group, SD Machine Learning you could create a summary slide for each section, and plan on spending less than a minute on each slide

1. Introduction: problem statement and executive summary
2. Data: where'd you get your data or other resources and what's in it)
3. EDA: plots and explanations of your data
4. Modeling or code: a hyperparameter table describing the various versions of your predictive model or software module you built
5. Future work: what would you do next if you had a couple more months
6. Conclusion/Insights: what did you learn about the world or about software development or data science
7. Demonstration?

### 2.2 Software Projects

1. Introduction: what does your software do?
2. Dependencies: what packages/libraries does your software require
3. User stories or features
4. High level functions or classes
5. Example test cases (doctests?)
6. Lessons learned
7. Demonstrate the command line app or web application

### 2.3 Conversation Design

1. Introduction: UX design. Example user stories.
2. Dialog tree or graph diagram
3. Intent recognition approach and examples
4. Demonstration

