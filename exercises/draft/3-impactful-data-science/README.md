# Machine Learning Factory

Process optimization with Machine Learning is a really powerful way to increase your prosocial impact.
For example, we use data science to help a nonprofit college counseling organization increase the number of disadvantaged students they can help graduate.

## Data Quest

In order to maintain the privacy of our clients and their beneficiaries, we've generated a synthetic dataset.
This way you can explore and try to find the clues in the data that lead you to some interventions to help more student graduate.

You'll have 3 tables to explore:

1. students
2. advisors
3. interactions

The interactions table includes the `student_id`s and `advisor_id`s from the other two tables. 
This will allow you to identify the details of every interaction between students and their advisors over the course of many years.
