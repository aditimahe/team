# Now what?

So you wrapped up a project and completed some exercises. Now what?

## Projects

If you are looking for another passion project, you can continue working on your internship project. 
You probably created a "future work" section. If not do that now to record all your ideas, for your future self.

If you want to shift gears and work on something new, you can look back the the [project ideas list](proai.org/project-ideas) for inspiration. 
And here are some other resources to give you project ideas:

- eulerproject.org
- Kaggle
- paperswithcode.com
- any chapter of _Artificial Intelligence: A Modern Approach_

## Courses

If you're looking for something more organized, like a course, you might check out one of these:

- Coursera: Some free college and graduate-level courses
- Udacity: Mostly paid courses
- Udemy
- opencourseware: Free classroom material from top notch MIT & Stanford courses

## Social

Be prosocial with your learning. Support people on open, ad-free, social networks.

- There's an ActivityPub network [for pretty much anything](https://awesomeopensource.com/projects/activitypub), book reviews, professional life, hobbies, etc: 
- Mastadon (microblog like Twitter only more prosocial)
- Reddit: some [subreddits](proai.org/prosocial-subreddits) are somewhat prosocial (https://gitlab.com/tangibleai/team/-/blob/master/exercises/1-prosocial-ai/prosocial-subreddits-and-users.md)
- Substack blogs that pay the writer directly (and no ads)
