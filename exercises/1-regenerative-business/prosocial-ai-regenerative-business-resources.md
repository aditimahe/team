# Regenerative Business and how it relates to Prosocial AI

## TODO

- [ ] understand [this article](https://en.wikipedia.org/wiki/Sustainable_business#cite_note-44)
- [ ] understand [this article](https://en.wikipedia.org/wiki/Sustainable_business#cite_note-45) 
- [ ] add 2 specific business models to the [Sustainable_business article on wikipedia](https://en.wikipedia.org/wiki/Sustainable_business#Circular_business_models)
- [ ] Eda & Jessa: research Sustainable brands live event in San Diego, conf in October!

## Biggest Competitive

- circular economy: A circular economy (also referred to as "circularity"[2]) is an economic system that tackles global challenges like climate change, biodiversity loss, waste, and pollution. 
- regenerative business: 

## NextSD resources

- [Next SD Vison - POLARIS by Jessa Spainhower and Isabel Mo](https://www.canva.com/design/DAEjlsU7Uf8/3Qf0CdDOT4qYh3lxEvSjkQ/view#2)
- [Resilience & Regeneration](./Resilience & Regeneration.pdf)
- [Towards Regenerative Development by Richard McDonald](./Thesis - Towards Regenerative Development by Richard McDonald.doc)
- [2015-Regenerative Capitalism](./2015-Regenerative-Capitalism-4-20-15-final.pdf)

## Questions
- Is "Regenerative Business" and economic theory, like "Doughnut Economics"?
- Or is it more of a philosophy or ethic or way of thinking, like "Prosocial Business" or "Authentic Business"
- How is the term used differently from other terms which are already wikipedia article titles: "Regenerative Capitalism", "Regenerative Economic Theory", etc.
- Is regenerative business a widely used term? 
- What does it mean?
- Should we start a Wikipedia article on it?  Are there synonym pages or ambiguity redirect or disambiguation pages that we could add to?
- What are the most popular search results for "Regenerative Business"? (Who is doing SEO on the term.)
- Are there any for-profit companies actively promoting the term and using it to market their products?
- Do any big tech companies "take a stand" on the concept?

## Related Wikipedia pages

### closest synonyms
- [Regenerative economic theory](https://en.wikipedia.org/wiki/Regenerative_economic_theory) only 2 paragraphs written in 1999
    - rename to Regenerative economics?
    - consolidate with "Regenerative capitalism"
- [Regenerative capitalism](https://en.wikipedia.org/w/index.php?title=Regenerative_capitalism&redirect=no) redirects to [John B. Fullerton](https://en.wikipedia.org/wiki/John_B._Fullerton)
- [Regenerative economic theory](https://en.wikipedia.org/wiki/Regenerative_economic_theory)
- [Regeneration_(biology)](https://en.wikipedia.org/wiki/Regeneration_(biology))

### Wikipedia "Regeneration" disambiguation

- [Regeneration (biology)](https://en.wikipedia.org/wiki/Regeneration_(biology) "Regeneration (biology)"), the ability to recreate lost or damaged cells, tissues, organs and limbs
-   [Regeneration (ecology)](https://en.wikipedia.org/wiki/Regeneration_(ecology) "Regeneration (ecology)"), the ability of ecosystems to regenerate biomass, using photosynthesis
-   [Regeneration in humans](https://en.wikipedia.org/wiki/Regeneration_in_humans "Regeneration in humans"), the ability of humans to recreate, or induce the regeneration of, lost tissue
-   [Regenerative (design)](https://en.wikipedia.org/wiki/Regenerative_(design) "Regenerative (design)"), a process for resilient and sustainable development
-   [Regenerative agriculture](https://en.wikipedia.org/wiki/Regenerative_agriculture "Regenerative agriculture"), a sub-category of organic agriculture

## Terminology


### "Regenerative Business" related terms

- Inclusive Capitalism
- Sustainable Capitalism
- Stakeholder Capitalism//Capitalists
- Regenerative Capitalism/Capitalists 
- Regenerative 
- Conscious Capitalism (John Mackey, Raj Sisodia, Trademarked)
- Firms of Endearment 
- [authentic marketing](https://www.georgekao.com/authentic-marketing.html)
- authentic leadership
- Supercooperators

- Sustainable business
- ESG (Environmental, Social, Governance)
- prosocial business


### Related technologies

- Zero sum game
- Mechanism design
- prosocial algorithms
- beneficial AI
- human compatible AI

## Resources

- [World Economic Forum explanation of Regenerative Business](https://www.weforum.org/agenda/2020/01/the-regenerative-business-approach-a-roadmap-for-rapid-change/)
- Capital Institute (by Carol Sanford?)
- [Carol Sanford](https://carolsanford.medium.com/ )
- [Dr. Tamsen - living systems]()

## Examples

- Malala - GirlUp summit
- Article 22 and Opolis Optics 
- Zeitgeist San Diego

## 4 week program in Regenerative Business

- presentation to Bogota
- 
