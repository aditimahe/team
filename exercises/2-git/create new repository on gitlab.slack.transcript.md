Eda FIRAT Yes, I am trying to upload but git connects with my Github account.How can I change to connect with gitlab?
Screen Shot 2021-03-16 at 16.51.26.png
Screen Shot 2021-03-16 at 16.51.26.png


8:54 AM
Dwayne Negron https://gitlab.com/tangibleai/team/-/tree/master/learning-resources/gitlab
8:54 AM
Eda, I have resource here for you to help connect your gitlab and make / handshake the SSH!
8:57 AM
Eda FIRAT Okay, thank you
12:16 PM
hobs Eda could you copy and paste the output from your terminal for the following command?
git remote -v
12:17 PM
Also this command:
ls -al
12:21 PM
I'm worried you are trying to push the rasa package rather than your personal rasa project (the directory containing the yml files that you edited)
12:27 PM
Eda FIRAT
Screen Shot 2021-03-16 at 20.27.18.png
Screen Shot 2021-03-16 at 20.27.18.png


:pray:
1

12:39 PM
hobs Its easier to work with for us if you paste the text, that way we can see much more of what is going on. It's a good habit to get into to always paste text target than screenshots, whenever possible.
12:40 PM
Eda FIRAT Ahh, yeah. Of course
12:40 PM
hobs looks like the ls command is showing that there is not a .git directory here
12:40 PM
that's a good thing.
12:41 PM
all you need to do is run these commands:
git init .
12:42 PM
Eda FIRAT
Untitled
(base) MacBook-Pro:rasa-playground eda$ git init .
Initialized empty Git repository in /Users/eda/code/rasa/rasa-playground/.git/
(base) MacBook-Pro:rasa-playground eda$


:pray:
1

12:42 PM
hobs and then go to gitlab.com and hit the plus sign to create a repository there
12:43 PM
awesome. looks like git init worked
12:43 PM
you probably want to do ls -al again to make sure you see the .git directory there
12:44 PM
Eda FIRAT I created which is called Chatbot Project.
12:44 PM
Yes, there is .git directory
12:44 PM
hobs cool. now copy the url for your project and paste it here so i can see it
12:45 PM
(for your Chatbot Project repo)
12:45 PM
Eda FIRAT Actually, I created as a new project.Inside is empty now. But I will try to send the projects with git commands
12:46 PM
hobs That's what I wanted.
12:46 PM
A new project.
12:46 PM
That's good.
12:46 PM
Eda FIRAT git status
12:46 PM
hobs What I need is the URL for the empty repo on gitlab.com
12:47 PM
Eda FIRAT Okay
12:47 PM
hobs it should look something like this: https://gitlab.com/yourname/chatbot-project
12:48 PM
Eda FIRAT https://gitlab.com/EdaFIRAT/chatbot-project.git
12:48 PM
hobs Excellent.
12:49 PM
Now we just need to link your local laptop repository by adding it as a remote:
12:49 PM
git remote add origin git@gitlab.com:EdaFIRAT/chabot-project.git
12:50 PM
Notice how I changed the / to a : and added git@ at the beginning.
12:50 PM
That's how you change a web url to an ssh URL.
12:51 PM
Eda FIRAT I see
12:52 PM
I pasted this remote command git remote add origin git@gitlab.com:EdaFIRAT/chabot-project.git in terminal.
12:52 PM
hobs Excellent. Now do git remote and paste here what you see.
12:52 PM
Sorry, git remote -v
12:53 PM
So we can see what remotes your local repository is linked to...
12:53 PM
Eda FIRAT
Untitled
(base) MacBook-Pro:rasa-playground eda$ git remote -v
origin  git@gitlab.com:EdaFIRAT/chabot-project.git (fetch)
origin  git@gitlab.com:EdaFIRAT/chabot-project.git (push)
(base) MacBook-Pro:rasa-playground eda$


12:53 PM
hobs Perfect!
12:54 PM
now try git push -u origin master
12:54 PM
Eda FIRAT
Untitled
(base) MacBook-Pro:rasa-playground eda$ git push -u origin master
error: src refspec master does not match any
error: failed to push some refs to 'git@gitlab.com:EdaFIRAT/chabot-project.git'
(base) MacBook-Pro:rasa-playground eda$


12:55 PM
hobs Ahh bummer. I forgot to add some files.
12:55 PM
Could you run git status and then add one of the files listed using the git add command? (edited)
12:56 PM
And thank you for pasting all the text. That will help me create another lesson file for future interns.
:raised_hands:
1

12:57 PM
Eda FIRAT
Untitled
(base) MacBook-Pro:rasa-playground eda$ git add .
(base) MacBook-Pro:rasa-playground eda$ git commit -m "I am uploading my chatbot project"
[master (root-commit) b79c749] I am uploading my chatbot project
 13 files changed, 605 insertions(+)
Click to expand inline (20 lines)


12:57 PM
Then, git push -u origin master right?
12:58 PM
hobs Yes! And since you've already tried -u origin master you can probably just run git push.
12:59 PM
Eda FIRAT Okay
12:59 PM
Eda FIRAT
Untitled
git push
fatal: The current branch master has no upstream branch.
To push the current branch and set the remote as upstream, use
    git push --set-upstream origin master
Click to expand inline (7 lines)


2 replies
Last reply today at 1:15 PMView thread
12:59 PM
hobs From now on when you type git push it will assume you mean git push origin master
1:00 PM
I was wrong. You need to use git push -u origin master to set the upstream branch to master on the remote called origin (gitlab). (edited)
1:06 PM
Eda FIRAT
Untitled
(base) MacBook-Pro:rasa-playground eda$ git push -u origin master
Enumerating objects: 18, done.
Counting objects: 100% (18/18), done.
Delta compression using up to 8 threads
Compressing objects: 100% (16/16), done.
Click to expand inline (24 lines)

>>>>> thread

24m
Eda FIRAT
Untitled
git push
fatal: The current branch master has no upstream branch.
To push the current branch and set the remote as upstream, use
    git push --set-upstream origin master
Click to expand inline (7 lines)


12m
hobs  It actually tells you that on line 5 of the error message. Often git will tell you what to do when things don't work.
New
8m
Eda FIRAT  I understand.Because it says
There is no upstream branch.
It means I have to add master branch.
(edited)
3m
hobs  FYI -u is the same as --set-upstream so git push -u origin master is the same as git push --set-upstream origin master. (edited)
2m
hobs  That's the command I was talking about on line 5 above.  That tells it that when you type git push next time to assume you mean git push origin master. That sets the default argument for that command git push. (edited)

<<<< end thread


1:11 PM
hobs Thank you
1:12 PM
Eda FIRAT Thank you, too !
1:13 PM
hobs I'll take a look and maybe run it myself to see if I can debug it.
New
1:17 PM
Eda FIRAT Okay
1:17 PM
hobs You need to go to sleep.
