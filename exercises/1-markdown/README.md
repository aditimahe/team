# Markdown Resources & Content

## Table of Contents
1. [What is Markdown?](#what-is-markdown)
2. [Install](#how-to-install)
    a. [Package Control Method of Installation](#package-control-method-of-installation)
3. [Uses and Application](#uses-and-application)
    a. [How to enable WYISWYG or What You See Is What You Get for .md](#how-to-enable-WYISWYG-or-what-you-see-is-what-you-get-for-.md)
    b. [How to Create and Edit Markdown Files Video](#how-to-create-and-edit-markdown-files-video)
4. [Advanced Techniques](#advanced-techniques)


### What is Markdown?

Markdown or .md is an incredibly flexible coding language that utilizes a simplified syntax. It is a powerful tool for any coder because of it's compatibility with many different systems, languages, and suites. At its most basic, Markdown can be used for efficient documentation that is flexible enough to be adapted into .pdf, .html, or .docx formats. 

### Install

While .md files can be created and managed using a multitude of software suites the preference at TangibleAI is to use [Sublime Text](@sublime-text). Installation of [Sublime Text](@sublime-text) can be found [here](@sublime-text). 

In order to best use .md the following packages will be installed into your [Sublime Text](@sublime-text). While links are provided for each package, we will be using the built in [package control](@sublime-text/package-control) to install them via [Sublime Text](@sublime-text).

* Markdown Editing - package which provides robust syntax highlighting. Link can be found [here](https://github.com/SublimeText-Markdown/MarkdownEditing). 
* MarkdownPreview - allows for the user to preview (build) your markdown simply and efficiently and can be found [here](https://facelessuser.github.io/MarkdownPreview/install/). 
* LiveReload - allows your preview of markdown in browser to auto update everytime the file is saved in [Sublime Text3](@sublime-text) and can be found [here](https://w3cssthemes.com/sublime-text-3-and-live-reload-with-auto-saving-features/)

#### Package Control Method of Installation

To install Markdown Editing, MarkdownPreview and LiveReload easily we will do the following steps: 
1. Windows/Linux - press ctrl + shift + p , Mac - press cmd + shift + p. This will bring up your "Palette" a tool used frequently in [Sublime Text](@sublime-text)
2. Type in the following and then press return (enter).
    ```
    install package
    ```
3. Type in the following and press return (enter). 
    ```
    Markdown Editing
    ```
4. Repeat step 2 and now type MarkdownPreview.
5. Repeat step 2 and now type LiveReload.
6. Restart your Sublime Text. 

### Uses and Application
#### How to enable WYISWYG or What You See Is What You Get for .md

The easiest way to enable WYSIWYG is via LiveReload in [Sublime Text](@sublime-text). In order to do this perform the following steps: 
1. Windows/Linux - press ctrl + shift + p , Mac - press cmd + shift + p
2. Type in the following and then press return.
    ```
    LiveReload Enable/Disable Plugins
    ```
3. Use your arrow keys to scroll down to or type in the following and then press return. 
    ```
    Enable Simple Reload
    ```
4. Re-open your palette using step 1. 
5. Type in the following and press return. 
    ```
    Preview in Browser
    ```

#### How to Create and Edit Markdown Files Video
[screenshot of vid](@vid01)
[How to create and edit markdown files in gitlab](https://tan.sfo2.digitaloceanspaces.com/videos/howto/onboarding-02-howto-create-and-edit-markdown-files-in-gitlab-hobs-2020-11-17.mp4)

    
### Advanced Techniques 