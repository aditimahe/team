# Datatype Detectors - an Automatic Transmission for Machine Learning

You can help us build our our Google-crushing AutoML pipeline just by refining the way we detect different kinds of ML data with this [Miniproject: Automatic ML Datatype Detector](https://gitlab.com/tangibleai/team/-/tree/master/learning-resources/projects/minproject--automatic-ml-datatype-detector.md)

