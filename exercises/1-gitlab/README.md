# GitLab Resources & Content

## Table of Contents
1. [What is GitLab?](#what-is-gitlab?)
    a. [TangibleAI.git](#tangibleai.git)
    b. [GitLab vs GitHub](#gitlab-vs-github)
2. [Install](#how-to-install)
    a. Windows (#windows)
    b. Linux (#linux)
    c. Mac (#mac)
3. [Uses and Application](#uses-and-application)
    a. [Connecting your PC to git via Terminal](#connecting-your-pc-to-git-via-terminal)
4. [Advanced Techniques](advanced-techniques)
    a. [Creating, Managing, and Working with a Directory](creating,-managing,-and-working-with-a-directory)

### What is Gitlab?

GitLab is a software developer platform similar to GitHub.
It allows for multiple users to contribute to the same project and files in parallel.
It uses the `git` application for version control.
This means almost everything you do in the GUI at [gitlab.com](gitlab.com), you can do from the command line.

#### [TangibleAI's GitLab Repositories](https://gitlab.com/tangibleai)

- [gitlab.com/tangibleai/team](https://gitlab.com/tangibleai/team)
- [gitlab.com/tangibleai/qary](https://gitlab.com/tangibleai/qary)
- [gitlab.com/tangibleai/proai](https://gitlab.com/tangibleai/proai)
- [gitlab.com/tangibleai/django-api](https://gitlab.com/tangibleai/django-api)
- [gitlab.com/tangibleai/tanbot](https://gitlab.com/tangibleai/tanbot)
- [gitlab.com/tangibleai/nlpia](https://gitlab.com/tangibleai/nlpia)

#### GitLab vs GitHub

Tangible AI has decided to go with GitLab over GitHub.
The primary difference between the two is that GitLab is fully open source.
This means you could download and install the GitLab web application on your own internal server.
And GitLab takes openness a step further by open sourcing even their business process documentation.
This helps you figure out the best way to use `git` and GitLab within your own organization.

### Install Git

In order to be able to use the `git` version control system you need to install it on your machine.
It comes built into Ubuntu (Linux) and Mac OSX (based on Linux).
But on Windows you'll have to manually install it...

#### Windows

1. Open up the [Anaconda Navigator](@conda)
2. Then click on the Powershell Prompt
    [Powershell Prompt](@installw01)
3. Type `git --version`
4. If you do not have git installed (likely) you should get the following pop up.
[git --version](@installw02)
5. You can download and install git [here](https://git-scm.com/downloads) download link
a. Before continuing, consider installing this program, [Sublime Text](@sublime-text). Think of Sublime Text as a Microsoft Word **FOR CODE**. It can be used with Python (just like Pycharm and many others). In the future, we may need it with git. [Link is here](https://www.sublimetext.com/3)
b. When installing git, you will see the following window change from VIM --> Sublime Text; refer to the image if neccessary.
[git sublime text](@installw03)
c. Then hit next until finished.
d. Reboot your computer.
6. Redo step 3.

Congratulations, git has been installed!


### Uses and Application
#### Using git via the Terminal
1. For this next step, you will need a GitLab account and the username of your account. In order to find it, go to the top right corner of the GitLab website after logging in. Your username is what is after the "@" symbol. Refer to the picture for more information.
[git account](@installw101)
2. With your username, open up powershell from the Anaconda Navigator.
3. Type the following command into the powershell and subsitute your username where applicable. Note: replace your_username with your username and likewise for your email; retain the double quotes in the commands.
```
git config --global user.name "your_username"
```
4. Now put in your email with the following command.
```
git config --global user.email "your_email_address@example.com"
```
5. Now let us check to see if the information is correct
```
git config --global --list
```
6. This is what the commands should look like in your powershell! Refer to the image
[git commands](@installw102)
7. Congratulations, we have now connected your username and email for GitLab with your PC. Next we have to create a unique password and link the two (GitLab and your PC), so that both can interact correctly!

#### Creating an SSH key

1. Find the Terminal application on your machine. In Ubuntu you can type [ctrl]
1. In your favorite Terminal, type in the following command to double check if you have the ability to create an SSH key.

```
ssh -V
```

2. You should see a response similar to the following:

```
OpenSSH_for_Windows_7.7p1, LibreSSL 2.6.5
```

3. Now we are going to create our special password (ssh key).
```
ssh-keygen
```
4. Press enter three times. This will save the ssh to a location and not lock it behind an additional password.
5. So far this is what we have! Refer to image
[ssh password](@installw301)
6. Now we need to copy our SSH Key to the clipboard in order to do that put in the following command

```console
cat ~/.ssh/id_ed25519.pub | clip
```

7. Your password is now copied! DO NOT COPY AND PASTE anything! Lets go straight to GitLab!
8. Click on the top right corner and go to your gitlab settings. Then we will look on the left side of the screen and click on SSH Keys. Refer to the image.
[ssh password location](@installw302)
9. Paste your key into the blank space labeled “key” do not put an expiration date, and then for the title use but whatever you call your pc! Now hit add key.
[adding an ssh key](@installw303)
10. Now let us perform a “handshake” and have your pc meet gitlab using the password. Put in the following code
```
ssh -T git@gitlab.com
```
11. It will ask you if you are sure?
a. Put yes.
12. You will see a congratulations message!


### Advanced Techniques

#### Creating, Managing, and Working with a Directory

[Video Thumbnail](@vid01)
[Video Link](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-bash-git-gitlab-nano-by-hobs-eda-2020-10-05.mp4)
