# Data Science with Django

1. create an empty django app (your first django app)
2. create 2 tables, Dataset (name, kind, num_features) and Vector2D (x, y, dataset=relationship with dataset)
3. create at least 8 data points (rows in the Vector2D table):
    y = height of your family members or coffee mugs and glasses in your home
    x = only one feature like weight, age, width/diameter, min width, max width, color, eye color, hair color, favorite sport, occupation
4. create VectorND or just Vector table that can hold unlimited number of features in a dictionary JsonField
5. install DjangoRestFramework and serve up your dataset
6. connect your data to playground.proai.org
